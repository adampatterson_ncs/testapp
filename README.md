# testapp

### compile

```sh
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o testapp .
```

### docker

```sh
docker build -t testapp .
```
